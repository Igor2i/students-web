package com.igor2i.students.modules.pojo.university.objects;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Component
@Entity(name = "users")
public class User {

  @Id
  @GeneratedValue
  private Long id;
  @Column(unique = true)
  private String login;
  @Column
  private String passwd;

  public User(User user) {
    this.id = user.getId();
    this.login = user.getLogin();
    this.passwd = user.getPasswd();

  }

  public User() {
  }

  public User(Long id, String user, String passwd) {
    this.id = id;
    this.login = user;
    this.passwd = passwd;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPasswd() {
    return passwd;
  }

  public void setPasswd(String passwd) {
    this.passwd = passwd;
  }

  @Override
  public String toString() {
    return "User{" +
            "id=" + id +
            ", user='" + login + '\'' +
            ", passwd='" + passwd + '\'' +
            '}';
  }
}
