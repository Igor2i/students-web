package com.igor2i.students.services;

import com.igor2i.students.modules.pojo.university.Roles;
import com.igor2i.students.modules.pojo.university.objects.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
 * Created by igor2i on 13.03.17.
 */
@Service
public class RoleService {

    private static final Logger LOGGER = LogManager.getLogger(UserService.class);

    private Roles roles;

    @Autowired
    public RoleService(Roles users) {
        this.roles = users;
    }


    public Role getByName(String login){
        Role role = null;
        try {
            role = roles.getByName(login);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return role;
    }

}
